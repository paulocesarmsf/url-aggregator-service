import requests
import pandas
import logging

from models.product import Product


class ProductManager(object):

    def url_product_is_200(self, image_url):
        return requests.get(image_url).status_code == 200

    def get_product(id):
        product = Product.objects(product_id=id).first()

        if not product:
            return Product(product_id=id)
        return product

    def save_dump_pics_products(self, dump_pics):
        for row in dump_pics.itertuples(index=False):
            id = row._asdict()['productId']
            product = ProductManager.get_product(id)

            if not len(product.images) == 3:
                self.save_pics_product(dump_pics, id, product)

            dump_pics = dump_pics[dump_pics['productId'] != id]

    def save_pics_product(self, dump_pics, id, product):
        df_specifc_product = dump_pics[dump_pics['productId'] == id]
        for specific_row in df_specifc_product.itertuples(index=False):
            image_url = specific_row._asdict()['image']

            if self.url_product_is_200(image_url):
                product.images.append(image_url)
            df_specifc_product = df_specifc_product[df_specifc_product['image'] != image_url]

            if len(product.images) == 3:
                product.save()
                break

    def read_dump(self, path):
        try:
            return pandas.read_json(path_or_buf=path, lines=True)
        except ValueError as e:
            logging.error('Path invalid: {path}'.format(path=path))
            logging.error('Error: {e}'.format(e=e))
