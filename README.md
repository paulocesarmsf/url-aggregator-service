## Steps for run local

1. git clone git@bitbucket.org:paulocesarmsf/url-aggregator-service.git
2. pip install -r requirements.txt (at root the project)
3. docker run --name mongodb -p 27017:27017 -d mongo


## Run tests
1. cd test
2. nose2 -v (at test folder)# url-aggregator-service


## Input Dump Example at root Run App
1. run app.py
