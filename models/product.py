from mongoengine import Document, StringField, ListField


class Product(Document):
    meta = {'collection': 'product'}

    product_id = StringField(required=True)
    images = ListField(default=[])

