from mongoengine import connect
from settings import MONGO_URI


class MongoDatabase:

    class __MongoInstance:
        def __init__(self, mongo_uri):
            connect('api_produtos', host=mongo_uri, maxpoolsize=10)

    started = False

    def __init__(self, mongo_uri=MONGO_URI):
        if not MongoDatabase.started:
            MongoDatabase.__MongoInstance(mongo_uri)
            MongoDatabase.started = True


def configure_mongo(mongo_url=MONGO_URI):
    MongoDatabase(mongo_url)
