import os

if os.getenv('CLUSTER_URL'):
    MONGO_URI = 'mongodb://{user}:{password}@{url}'\
        .format(user=os.getenv('MONGO_USER'),
                password=os.getenv('MONGO_PASSWORD'),
                url=os.getenv('CLUSTER_URL'))
else:
    MONGO_URI = 'mongodb://{url}:27017'.format(url=os.getenv('MONGO_URL', 'localhost'))
