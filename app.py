from models.mongo_database import configure_mongo
from workers.product_manager import ProductManager

product_manager =ProductManager()

configure_mongo()
dump_pics = product_manager.read_dump('input-dump.json')
product_manager.save_dump_pics_products(dump_pics)
