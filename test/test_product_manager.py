# -*- coding: utf-8 -*-
import unittest
from mongoengine import connect
import requests_mock

from models.product import Product
from workers.product_manager import ProductManager


class TestProductManager(unittest.TestCase):

    def setUp(self):
        connect('mongoenginetest', host='mongomock://localhost')

    @requests_mock.Mocker()
    def test_pip_url_status_200_true(self, m):
        pic_url = 'http://www.xxx.com/1.png'
        product_manager = ProductManager()
        m.get(pic_url, status_code=200)

        self.assertTrue(product_manager.url_product_is_200(pic_url))

    @requests_mock.Mocker()
    def test_pip_url_status_200_false(self, m):
        pic_url = 'http://www.xxx.com/1.png'
        product_manager = ProductManager()
        m.get(pic_url, status_code=404)

        self.assertFalse(product_manager.url_product_is_200(pic_url))

    def test_it_has_three_save_pics(self):
        product_manager = ProductManager()
        Product(product_id='pid1', images=["http://www.xxx.com/1.png",
                                           "http://www.xxx.com/2.png",
                                           "http://www.xxx.com/3.png"]).save()
        dump_test = product_manager.read_dump('dump_test.json')
        product_manager.save_dump_pics_products(dump_test)

        self.assertEqual(3, len(list(Product.objects(product_id='pid1').first().images)))

    @requests_mock.Mocker()
    def test_it_has_zero_save_pics(self, m):
        product_manager = ProductManager()
        p = Product(product_id='pid1', images=[]).save()

        pic_url_1 = 'http://localhost:4567/images/1.png'
        pic_url_2 = 'http://localhost:4567/images/2.png'
        pic_url_3 = 'http://localhost:4567/images/3.png'

        m.get(pic_url_1, status_code=200),
        m.get(pic_url_2, status_code=200),
        m.get(pic_url_3, status_code=200),

        dump_test = product_manager.read_dump('dump_test.json')
        product_manager.save_pics_product(dump_test, p.product_id, p)

        self.assertEqual(3, len(list(Product.objects(product_id='pid1').first().images)))